import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const FourthPage = () => (
  <Layout>
    <SEO title="Page three" />
    <h1>Greetings from the third page my dude</h1>
    <p>Welcome to page 4</p>
    <Link to="/">Go back to the homepage</Link>
    <Link to="/page-3/">or page 3</Link>
  </Layout>
)

export default FourthPage
